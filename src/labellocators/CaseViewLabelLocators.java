package labellocators;

import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.LabelLocatorStrategy;
import com.provar.core.testapi.annotations.StrategyLocator;

@LabelLocatorStrategy(title = "Case View Label Locator",
		priority = Integer.MAX_VALUE,
		labelLocators = {
				@StrategyLocator(name = "labelLocator1", findBy = @FindBy(xpath = "div[contains(@class, 'label')]/span")),
				@StrategyLocator(name = "labelLocator2", findBy = @FindBy(xpath = "label[contains(@class,'inputLabel')]/span")),
				@StrategyLocator(name = "labelLocator3", findBy = @FindBy(xpath = "span[contains(@class, 'label')]/span[text()]")),
				@StrategyLocator(name = "labelLocator4", findBy = @FindBy(xpath = "label[contains(@class,'uiLabel')]/span")),
				@StrategyLocator(name = "labelLocator5", findBy = @FindBy(xpath = "label[contains(@class,'label')]")),
				
				
				},
		controlLocators = {
				
				@StrategyLocator(name = "controlWithoutValue1", findBy = @FindBy(xpath = "div[contains(@class, 'control')]/span")),
				@StrategyLocator(name = "controlWithoutValue2", findBy = @FindBy(xpath = "input[contains(@class,'input')]")),
			
				@StrategyLocator(name = "controlWithValue1", findBy = @FindBy(xpath = "div/input[contains(@class,'input')]")),
				@StrategyLocator(name = "controlWithValue2", findBy = @FindBy(xpath = "div[contains(@class,'control')]//span[text()]")),
				
				@StrategyLocator(name = "controlWithPicklist", findBy = @FindBy(xpath = "div[contains(@class,'uiMenu')]//a[@class='select']")),
				
				@StrategyLocator(name = "controlWithTextArea", findBy = @FindBy(xpath = "textarea[contains(@class,'textarea')]")),
				
				@StrategyLocator(name = "controlWithCheckbox", findBy = @FindBy(xpath = "input[contains(@type,'checkbox')]")),
				
				
				@StrategyLocator(name = "controlWithLookup", findBy = @FindBy(xpath = "div//input[contains(@class,'uiInput')]")),
				
				@StrategyLocator(name = "controlWithAddress", findBy = @FindBy(xpath = "div[contains(@class, 'control')]/span//div[contains(@class,'Address')]")),
				
				@StrategyLocator(name = "controlWithDateTime", findBy = @FindBy(xpath = "input[contains(@class,'input')]")),
				})
public class CaseViewLabelLocators { 

}
