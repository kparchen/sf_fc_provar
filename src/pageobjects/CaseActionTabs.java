package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@Page( title="CaseActionTabs"                                
     , summary=""
     , relativeUrl=""
     , connection="kparchen_QA"
     )             
public class CaseActionTabs {

	@LinkType()
	@FindBy(linkText = "Create Lead")
	public WebElement createLead;
	@TextType()
	@FindByLabel(label = "First Name")
	public WebElement firstName;
	@TextType()
	@FindByLabel(label = "Last Name")
	public WebElement lastName;
	@TextType()
	@FindByLabel(label = "Phone Number")
	public WebElement phoneNumber;
	@TextType()
	@FindByLabel(label = "MobilePhone")
	public WebElement mobilePhone;
	@TextType()
	@FindByLabel(label = "Email")
	public WebElement email;
	@TextType()
	@FindBy(xpath = "//div[text()=\"Description\"]/following::textarea")
	public WebElement description;
	@ButtonType()
	@FindByLabel(label = "Next")
	public WebElement next;
	@ButtonType()
	@FindBy(xpath = "//button[text()=\"Next\"]")
	public WebElement next1;
	@ButtonType()
	@FindBy(xpath = "//button[text()=\"Next\"]")
	public WebElement next2;
			
}
