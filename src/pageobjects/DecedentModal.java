package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pagestrategies.*;
import com.provar.core.testapi.annotations.*;

@Page( title="DecedentModal"                                
     , summary=""
     , relativeUrl=""
     , connection="kparchen_QA"
     )             
public class DecedentModal {

	@TextType()
	@FindBy(xpath = "//input[contains(@class,'firstName')]")
	public WebElement firstName;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='Middle Name']/following-sibling::input[contains(@class,'middleName')]")
	public WebElement middleName;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='Last Name*']/following-sibling::input[contains(@class,'lastName')]")
	public WebElement lastName;
	@TextType()
	@FindBy(xpath = "//label[normalize-space(.)='Suffix']/following-sibling::input[contains(@class,'suffix')]")
	public WebElement suffix;
	
			
}
