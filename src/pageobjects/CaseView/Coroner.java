package pageobjects.CaseView;

import java.util.List;
import labellocators.*;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pagestrategies.*;
import com.provar.core.testapi.annotations.*;

@Page( title="Coroner"                                
     , summary=""
     , relativeUrl=""
     , connection="Gaggan_QA"
     )             
public class Coroner {

	@PageBlock()
	public static class CaseViewSection {

		@ButtonType()
		@FindBy(xpath = ".//button[normalize-space(.)='Coroner' and @type='button']")
		public WebElement coroner;
		@TextType()
		@FindByLabel(label = "Coroner Case", controlLocator = "controlWithoutValue1", labelLocator = "labelLocator1", strategy = CaseViewLabelLocators.class)
		public WebElement coronerCase;
		@TextType()
		@FindByLabel(label = "Coroner Case", controlLocator = "controlWithPicklist", labelLocator = "labelLocator3", strategy = CaseViewLabelLocators.class)
		public WebElement coronerCaseValue;
		@TextType()
		@FindByLabel(label = "Coroner Number", controlLocator = "controlWithoutValue2", labelLocator = "labelLocator2", strategy = CaseViewLabelLocators.class)
		public WebElement coronerNumber;
		@TextType()
		@FindByLabel(label = "Coroner", controlLocator = "controlWithLookup", labelLocator = "labelLocator2", strategy = CaseViewLabelLocators.class)
		public WebElement CoronerAccount;
		@TextType()
		@FindByLabel(label = "Coroner Charges", controlLocator = "controlWithoutValue2", labelLocator = "labelLocator2", strategy = CaseViewLabelLocators.class)
		public WebElement CoronerCharges;
		@TextType()
		@FindByLabel(label = "Release Status: Coroner", controlLocator = "controlWithPicklist", labelLocator = "labelLocator3", strategy = CaseViewLabelLocators.class)
		public WebElement releaseStatusCoroner;
		@TextType()
		@FindByLabel(label = "Coroner Scheduled Release", controlLocator = "controlWithPicklist", labelLocator = "labelLocator3", strategy = CaseViewLabelLocators.class)
		public WebElement coronerScheduledRelease;
		@TextType()
		@FindByLabel(label = "Add Coroner Fees", controlLocator = "controlWithPicklist", labelLocator = "labelLocator3", strategy = CaseViewLabelLocators.class)
		public WebElement addCoronerFees;
		@ButtonType()
		@FindBy(xpath = "(//button[normalize-space(.)='Save' and @title='Save' and @type='button'])[1]")
		public WebElement saveInfo;
		@PageWait.Field(timeoutSeconds = 10)
		@TextType()
		@FindByLabel(label = "Account Name", controlLocator = "controlWithValue2", labelLocator = "labelLocator1", strategy = CaseViewLabelLocators.class)
		public WebElement Accountname;
		@TextType()
		@FindByLabel(label = "Account Name", controlLocator = "controlWithoutValue2", labelLocator = "labelLocator2", strategy = CaseViewLabelLocators.class)
		public WebElement Accountnamevalue;
		@TextType()
		@FindByLabel(label = "Coroner Fee", controlLocator = "controlWithoutValue2", labelLocator = "labelLocator2", strategy = CaseViewLabelLocators.class)
		public WebElement CoronerFeeValue;
		@TextType()
		@FindByLabel(label = "Phone", controlLocator = "controlWithoutValue2", labelLocator = "labelLocator2", strategy = CaseViewLabelLocators.class)
		public WebElement Accountphone;
		@TextType()
		@FindByLabel(label = "Coroner Sign-Out", controlLocator = "controlWithoutValue2", labelLocator = "labelLocator2", strategy = CaseViewLabelLocators.class)
		public WebElement CoronerSignOut;
		@TextType()
		@FindByLabel(label = "Emergency Phone", controlLocator = "controlWithoutValue2", labelLocator = "labelLocator2", strategy = CaseViewLabelLocators.class)
		public WebElement emergencyPhone;
		@TextType()
		@FindByLabel(label = "Coroner Fee Notes", controlLocator = "controlWithoutValue2", labelLocator = "labelLocator2", strategy = CaseViewLabelLocators.class)
		public WebElement coronerFeeNotes;
		@TextType()
		@FindByLabel(label = "Fax", controlLocator = "controlWithoutValue2", labelLocator = "labelLocator2", strategy = CaseViewLabelLocators.class)
		public WebElement fax;
		@TextType()
		@FindByLabel(label = "Coroner Release Times", controlLocator = "controlWithTextArea", labelLocator = "labelLocator2", strategy = CaseViewLabelLocators.class)
		public WebElement coronerReleaseTimes;
		@TextType()
		@FindByLabel(label = "Residential Death", controlLocator = "controlWithoutValue2", labelLocator = "labelLocator2", strategy = CaseViewLabelLocators.class)
		public WebElement residentialDeath;
		@TextType()
		@FindByLabel(label = "Billing Street", controlLocator = "controlWithTextArea", labelLocator = "labelLocator4", strategy = CaseViewLabelLocators.class)
		public WebElement BillingStreet;
		@TextType()
		@FindByLabel(label = "Billing City", controlLocator = "controlWithoutValue2", labelLocator = "labelLocator4", strategy = CaseViewLabelLocators.class)
		public WebElement billingCity;
		@TextType()
		@FindByLabel(label = "Billing State/Province", controlLocator = "controlWithoutValue2", labelLocator = "labelLocator2", strategy = CaseViewLabelLocators.class)
		public WebElement billingStateProvince;
		@TextType()
		@FindByLabel(label = "Billing Zip/Postal Code", controlLocator = "controlWithoutValue2", labelLocator = "labelLocator4", strategy = CaseViewLabelLocators.class)
		public WebElement billingZipPostalCode;
		@TextType()
		@FindByLabel(label = "Billing Country", controlLocator = "controlWithoutValue2", labelLocator = "labelLocator2", strategy = CaseViewLabelLocators.class)
		public WebElement billingCountry;
		@ButtonType()
		@FindBy(xpath = "(//button[normalize-space(.)='Save' and @title='Save' and @type='button'])[2]")
		public WebElement save;
		@TextType()
		@FindByLabel(label = "Account Name", controlLocator = "controlWithValue2", labelLocator = "labelLocator1", strategy = CaseViewLabelLocators.class)
		public WebElement Accountnamek;
	}

	@FindByBlock(label = "Coroner", labelOperator = Operator.Equals, pageSectionStrategy = CaseViewSectionStrategy.class)
	public CaseViewSection caseViewSection;
			
}
