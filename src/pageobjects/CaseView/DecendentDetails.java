package pageobjects.CaseView;


import labellocators.*;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pagestrategies.*;
import com.provar.core.testapi.annotations.*;

@Page( title="Decendent Details"                                
     , summary=""
     , relativeUrl=""
     , connection="Gaggan_QA"
     )             
public class DecendentDetails {

	@PageBlock()
	public static class CaseViewSection {

		@PageWait.Field(timeoutSeconds = 10)
		@TextType()
		@FindByLabel(label = "Pronounced By", controlLocator = "controlWithoutValue1", labelLocator = "labelLocator1", strategy = CaseViewLabelLocators.class)
		public WebElement pronouncedBy;
		@PageWait.Field(timeoutSeconds = 10)
		@TextType()
		@FindByLabel(label = "Pronounced By", controlLocator = "controlWithoutValue2", labelLocator = "labelLocator2", strategy = CaseViewLabelLocators.class)
		public WebElement PronouncedBy1;
		@TextType()
		@FindByLabel(label = "Pronounced by Phone", controlLocator = "controlWithoutValue2", labelLocator = "labelLocator2", strategy = CaseViewLabelLocators.class)
		public WebElement PronouncedByPhone;
		@TextType()
		@FindByLabel(label = "Time of Death", controlLocator = "controlWithoutValue2", labelLocator = "labelLocator2", strategy = CaseViewLabelLocators.class)
		public WebElement timeOfDeath;
		@PageWait.BackgroundActivity(timeoutSeconds = 10)
		@TextType()
		@FindByLabel(label = "Date of Death", controlLocator = "controlWithValue1", labelLocator = "labelLocator2", strategy = CaseViewLabelLocators.class)
		public WebElement dateOfDeath;
		@ButtonType()
		@FindBy(xpath = "(//button[normalize-space(.)='Save' and @title='Save' and @type='button'])[1]")
		public WebElement SaveDD;
		@ButtonType()
		@FindBy(xpath = "//button[normalize-space(.)='Decedent Details' and @type='button']")
		public WebElement decedentDetails;
		@TextType()
		@FindByLabel(label = "Last, First", controlLocator = "controlWithValue2", labelLocator = "labelLocator1", strategy = CaseViewLabelLocators.class)
		public WebElement lastFirst;
		@TextType()
		@FindByLabel(label = "Account Name", controlLocator = "controlWithoutValue1", labelLocator = "labelLocator1", strategy = CaseViewLabelLocators.class)
		public WebElement accountName;
		@PageWait.Field(timeoutSeconds = 10)
		@TextType()
		@FindByLabel(label = "Salutation", controlLocator = "controlWithPicklist", labelLocator = "labelLocator3", strategy = CaseViewLabelLocators.class)
		public WebElement salutation;
		@TextType()
		@FindByLabel(label = "First Name", controlLocator = "controlWithoutValue2", labelLocator = "labelLocator4", strategy = CaseViewLabelLocators.class)
		public WebElement firstName;
		@TextType()
		@FindByLabel(label = "Middle Name", controlLocator = "controlWithoutValue2", labelLocator = "labelLocator4", strategy = CaseViewLabelLocators.class)
		public WebElement middleName;
		@TextType()
		@FindByLabel(label = "Last Name", controlLocator = "controlWithoutValue2", labelLocator = "labelLocator4", strategy = CaseViewLabelLocators.class)
		public WebElement lastName;
		@TextType()
		@FindByLabel(label = "Suffix", controlLocator = "controlWithoutValue2", labelLocator = "labelLocator4", strategy = CaseViewLabelLocators.class)
		public WebElement suffix;
		@PageWait.BackgroundActivity(timeoutSeconds = 60)
		@TextType()
		@FindByLabel(label = "Date of Birth", controlLocator = "controlWithValue1", labelLocator = "labelLocator2", strategy = CaseViewLabelLocators.class)
		public WebElement dateOfBirth;
		@LinkType()
		@FindByLabel(label = "Gender", controlLocator = "controlWithPicklist", labelLocator = "labelLocator3", strategy = CaseViewLabelLocators.class)
		public WebElement gender;
		@LinkType()
		@FindByLabel(label = "Age Category", controlLocator = "controlWithPicklist", labelLocator = "labelLocator3", strategy = CaseViewLabelLocators.class)
		public WebElement ageCategory;
		@TextType()
		@FindByLabel(label = "Billing Street", controlLocator = "controlWithTextArea", labelLocator = "labelLocator4", strategy = CaseViewLabelLocators.class)
		public WebElement billingStreet;
		@TextType()
		@FindByLabel(label = "Billing City", controlLocator = "controlWithoutValue2", labelLocator = "labelLocator4", strategy = CaseViewLabelLocators.class)
		public WebElement billingCity;
		@TextType()
		@FindByLabel(label = "Billing State/Province", controlLocator = "controlWithoutValue2", labelLocator = "labelLocator2", strategy = CaseViewLabelLocators.class)
		public WebElement billingStateProvince;
		@TextType()
		@FindByLabel(label = "Billing Zip/Postal Code", controlLocator = "controlWithoutValue2", labelLocator = "labelLocator4", strategy = CaseViewLabelLocators.class)
		public WebElement billingZipPostalCode;
		@TextType()
		@FindByLabel(label = "Billing Country", controlLocator = "controlWithoutValue2", labelLocator = "labelLocator2", strategy = CaseViewLabelLocators.class)
		public WebElement billingCountry;
		@TextType()
		@FindByLabel(label = "Residence Street", controlLocator = "controlWithTextArea", labelLocator = "labelLocator4", strategy = CaseViewLabelLocators.class)
		public WebElement residenceStreet;
		@TextType()
		@FindByLabel(label = "Residence City", controlLocator = "controlWithoutValue2", labelLocator = "labelLocator4", strategy = CaseViewLabelLocators.class)
		public WebElement residenceCity;
		@TextType()
		@FindByLabel(label = "Residence State/Province", controlLocator = "controlWithoutValue2", labelLocator = "labelLocator2", strategy = CaseViewLabelLocators.class)
		public WebElement residenceStateProvince;
		@TextType()
		@FindByLabel(label = "Residence Zip/Postal Code", controlLocator = "controlWithoutValue2", labelLocator = "labelLocator4", strategy = CaseViewLabelLocators.class)
		public WebElement residenceZipPostalCode;
		@TextType()
		@FindByLabel(label = "Residence Country", controlLocator = "controlWithoutValue2", labelLocator = "labelLocator2", strategy = CaseViewLabelLocators.class)
		public WebElement residenceCountry;
		@LinkType()
		@FindByLabel(label = "Employee", controlLocator = "controlWithPicklist", labelLocator = "labelLocator3", strategy = CaseViewLabelLocators.class)
		public WebElement employee;
		@BooleanType()
		@FindByLabel(label = "Pronounced By Same as Reported By", controlLocator = "controlWithCheckbox", labelLocator = "labelLocator2", strategy = CaseViewLabelLocators.class)
		public WebElement pronouncedBySameAsReportedBy;
		@TextType()
		@FindByLabel(label = "Pronounced by Mobile", controlLocator = "controlWithoutValue2", labelLocator = "labelLocator2", strategy = CaseViewLabelLocators.class)
		public WebElement pronouncedByMobile;
		@ButtonType()
		@FindBy(xpath = "(//button[normalize-space(.)='Save' and @title='Save' and @type='button'])[2]")
		public WebElement SavePB;
		@TextType()
		@FindBy(xpath = "(//label[normalize-space(.)='Date of Death']/following-sibling::div//input[@type='text'])[2]")
		public WebElement dateOfDeathPro;
		
	}

	@FindByBlock(label = "Decedent Details", labelOperator = Operator.Equals, pageSectionStrategy = CaseViewSectionStrategy.class)
	public CaseViewSection caseViewSection;


	
			
}
