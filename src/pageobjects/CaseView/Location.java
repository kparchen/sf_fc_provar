package pageobjects.CaseView;

import java.util.List;
import labellocators.*;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pagestrategies.*;
import com.provar.core.testapi.annotations.*;

@Page( title="Location"                                
     , summary=""
     , relativeUrl=""
     , connection="Gaggan_QA"
     )             
public class Location {

	@PageBlock()
	public static class CaseViewSection {

		@ButtonType()
		@FindBy(xpath = ".//button[normalize-space(.)='Locations (Current, Place of Death, Usual Residence)' and @type='button']")
		public WebElement locationsCurrentPlaceOfDeathUsualResidence;
		@PageWait.BackgroundActivity(timeoutSeconds = 60)
		@TextType()
		@FindByLabel(label = "Current Location (Lookup)", controlLocator = "controlWithoutValue1", labelLocator = "labelLocator1", strategy = CaseViewLabelLocators.class)
		public WebElement currentLocationLookup;
		@TextType()
		@FindByLabel(label = "Current Location (Lookup)", controlLocator = "controlWithLookup", labelLocator = "labelLocator2", strategy = CaseViewLabelLocators.class)
		public WebElement CurrentLocAccount;
		@LinkType()
		@FindByLabel(label = "Current Location Same As", controlLocator = "controlWithPicklist", labelLocator = "labelLocator3", strategy = CaseViewLabelLocators.class)
		public WebElement currentLocationSameAs;
		@TextType()
		@FindByLabel(label = "Current Location of Remains", controlLocator = "controlWithoutValue2", labelLocator = "labelLocator2", strategy = CaseViewLabelLocators.class)
		public WebElement currentLocationOfRemains;
		@TextType()
		@FindByLabel(label = "Current Location of Remains - Street", controlLocator = "controlWithoutValue2", labelLocator = "labelLocator2", strategy = CaseViewLabelLocators.class)
		public WebElement currentLocationOfRemainsStreet;
		@TextType()
		@FindByLabel(label = "Current Location of Remains - City", controlLocator = "controlWithoutValue2", labelLocator = "labelLocator2", strategy = CaseViewLabelLocators.class)
		public WebElement currentLocationOfRemainsCity;
		@TextType()
		@FindByLabel(label = "Current Location of Remains - State", controlLocator = "controlWithoutValue2", labelLocator = "labelLocator2", strategy = CaseViewLabelLocators.class)
		public WebElement currentLocationOfRemainsState;
		@TextType()
		@FindByLabel(label = "Current Location of Remains - Zip", controlLocator = "controlWithoutValue2", labelLocator = "labelLocator2", strategy = CaseViewLabelLocators.class)
		public WebElement currentLocationOfRemainsZip;
		@TextType()
		@FindByLabel(label = "Current Location of Remains - Country", controlLocator = "controlWithoutValue2", labelLocator = "labelLocator2", strategy = CaseViewLabelLocators.class)
		public WebElement currentLocationOfRemainsCountry;
		@TextType()
		@FindByLabel(label = "Current Location of Remains - Phone", controlLocator = "controlWithoutValue2", labelLocator = "labelLocator2", strategy = CaseViewLabelLocators.class)
		public WebElement currentLocationOfRemainsPhone;
		@ButtonType()
		@FindBy(xpath = "(//button[normalize-space(.)='Save' and @title='Save' and @type='button'])")
		public WebElement save;
		@PageWait.BackgroundActivity(timeoutSeconds = 60)
		@TextType()
		@FindByLabel(label = "Place of Death (Lookup)", controlLocator = "controlWithoutValue1", labelLocator = "labelLocator1", strategy = CaseViewLabelLocators.class)
		public WebElement placeOfDeathLookup;
		@TextType()
		@FindByLabel(label = "Place of Death (Lookup)", controlLocator = "controlWithLookup", labelLocator = "labelLocator2", strategy = CaseViewLabelLocators.class)
		public WebElement PlaceOfDeath;
		@LinkType()
		@FindByLabel(label = "Place of Death Same As", controlLocator = "controlWithPicklist", labelLocator = "labelLocator3", strategy = CaseViewLabelLocators.class)
		public WebElement placeOfDeathSameAs;
		@TextType()
		@FindByLabel(label = "Place of Death", controlLocator = "controlWithoutValue2", labelLocator = "labelLocator2", strategy = CaseViewLabelLocators.class)
		public WebElement placeOfDeath;
		@TextType()
		@FindByLabel(label = "Place of Death - Street", controlLocator = "controlWithoutValue2", labelLocator = "labelLocator2", strategy = CaseViewLabelLocators.class)
		public WebElement placeOfDeathStreet;
		@TextType()
		@FindByLabel(label = "Place of Death - City", controlLocator = "controlWithoutValue2", labelLocator = "labelLocator2", strategy = CaseViewLabelLocators.class)
		public WebElement placeOfDeathCity;
		@TextType()
		@FindByLabel(label = "Place of Death - State", controlLocator = "controlWithoutValue2", labelLocator = "labelLocator2", strategy = CaseViewLabelLocators.class)
		public WebElement placeOfDeathState;
		@TextType()
		@FindByLabel(label = "Place of Death - Zip", controlLocator = "controlWithoutValue2", labelLocator = "labelLocator2", strategy = CaseViewLabelLocators.class)
		public WebElement placeOfDeathZip;
		@TextType()
		@FindByLabel(label = "Place of Death - County", controlLocator = "controlWithPicklist", labelLocator = "labelLocator3", strategy = CaseViewLabelLocators.class)
		public WebElement placeOfDeathCounty;
		@TextType()
		@FindByLabel(label = "Place of Death - Country", controlLocator = "controlWithoutValue2", labelLocator = "labelLocator2", strategy = CaseViewLabelLocators.class)
		public WebElement placeOfDeathCountry;
		@TextType()
		@FindByLabel(label = "Place of Death - Phone", controlLocator = "controlWithoutValue2", labelLocator = "labelLocator2", strategy = CaseViewLabelLocators.class)
		public WebElement placeOfDeathPhone;
		@TextType()
		@FindByLabel(label = "Hospital Admission Status", controlLocator = "controlWithPicklist", labelLocator = "labelLocator3", strategy = CaseViewLabelLocators.class)
		public WebElement hospitalAdmissionStatus;
		@ButtonType()
		@FindBy(xpath = "(//button[normalize-space(.)='Save' and @title='Save' and @type='button'])[2]")
		public WebElement SavePOD;
		@PageWait.BackgroundActivity(timeoutSeconds = 60)
		@TextType()
		@FindByLabel(label = "Usual Residence Same As", controlLocator = "controlWithoutValue1", labelLocator = "labelLocator1", strategy = CaseViewLabelLocators.class)
		public WebElement usualResidenceSameAs;
		@LinkType()
		@FindByLabel(label = "Usual Residence Same As", controlLocator = "controlWithPicklist", labelLocator = "labelLocator3", strategy = CaseViewLabelLocators.class)
		public WebElement usualResidence;
		@TextType()
		@FindByLabel(label = "Decedent’s Usual Residence - Street", controlLocator = "controlWithoutValue2", labelLocator = "labelLocator2", strategy = CaseViewLabelLocators.class)
		public WebElement decedentSUsualResidenceStreet;
		@TextType()
		@FindByLabel(label = "Decedent’s Usual Residence - City", controlLocator = "controlWithoutValue2", labelLocator = "labelLocator2", strategy = CaseViewLabelLocators.class)
		public WebElement decedentSUsualResidenceCity;
		@TextType()
		@FindByLabel(label = "Decedent’s Usual Residence - State", controlLocator = "controlWithoutValue2", labelLocator = "labelLocator2", strategy = CaseViewLabelLocators.class)
		public WebElement decedentSUsualResidenceState;
		@TextType()
		@FindByLabel(label = "Decedent’s Usual Residence - Zip", controlLocator = "controlWithoutValue2", labelLocator = "labelLocator2", strategy = CaseViewLabelLocators.class)
		public WebElement decedentSUsualResidenceZip;
		@TextType()
		@FindByLabel(label = "Decedent’s Usual Residence - County", controlLocator = "controlWithoutValue2", labelLocator = "labelLocator2", strategy = CaseViewLabelLocators.class)
		public WebElement decedentSUsualResidenceCounty;
		@TextType()
		@FindByLabel(label = "Decedent’s Usual Residence - Country", controlLocator = "controlWithoutValue2", labelLocator = "labelLocator2", strategy = CaseViewLabelLocators.class)
		public WebElement decedentUsualResiCountry;
		@TextType()
		@FindByLabel(label = "Decedent’s Usual Residence - Yrs in Cnty", controlLocator = "controlWithoutValue2", labelLocator = "labelLocator2", strategy = CaseViewLabelLocators.class)
		public WebElement DecendentsYrsInCnty;
		@ButtonType()
		@FindBy(xpath = "(//button[normalize-space(.)='Save' and @title='Save' and @type='button'])[3]")
		public WebElement saveUR;
		@PageWait.BackgroundActivity(timeoutSeconds = 60)
		@TextType()
		@FindByLabel(label = "Billing Address", controlLocator = "controlWithAddress", labelLocator = "labelLocator1", strategy = CaseViewLabelLocators.class)
		public WebElement billingAddress;
		@TextType()
		@FindByLabel(label = "Billing Street", controlLocator = "controlWithTextArea", labelLocator = "labelLocator4", strategy = CaseViewLabelLocators.class)
		public WebElement billingStreet;
		@TextType()
		@FindByLabel(label = "Billing City", controlLocator = "controlWithoutValue2", labelLocator = "labelLocator4", strategy = CaseViewLabelLocators.class)
		public WebElement billingCity;
		@TextType()
		@FindByLabel(label = "Billing State/Province", controlLocator = "controlWithoutValue2", labelLocator = "labelLocator2", strategy = CaseViewLabelLocators.class)
		public WebElement billingStateProvince;
		@TextType()
		@FindByLabel(label = "Billing Zip/Postal Code", controlLocator = "controlWithoutValue2", labelLocator = "labelLocator4", strategy = CaseViewLabelLocators.class)
		public WebElement billingZipPostalCode;
		@TextType()
		@FindByLabel(label = "Billing Country", controlLocator = "controlWithoutValue2", labelLocator = "labelLocator2", strategy = CaseViewLabelLocators.class)
		public WebElement billingCountry;
		@TextType()
		@FindByLabel(label = "Residence Street", controlLocator = "controlWithTextArea", labelLocator = "labelLocator4", strategy = CaseViewLabelLocators.class)
		public WebElement residenceStreet;
		@TextType()
		@FindByLabel(label = "Residence City", controlLocator = "controlWithoutValue2", labelLocator = "labelLocator4", strategy = CaseViewLabelLocators.class)
		public WebElement residenceCity;
		@TextType()
		@FindByLabel(label = "Residence State/Province", controlLocator = "controlWithoutValue2", labelLocator = "labelLocator2", strategy = CaseViewLabelLocators.class)
		public WebElement residenceStateProvince;
		@TextType()
		@FindByLabel(label = "Residence Zip/Postal Code", controlLocator = "controlWithoutValue2", labelLocator = "labelLocator4", strategy = CaseViewLabelLocators.class)
		public WebElement residenceZipPostalCode;
		@TextType()
		@FindByLabel(label = "Residence Country", controlLocator = "controlWithoutValue2", labelLocator = "labelLocator2", strategy = CaseViewLabelLocators.class)
		public WebElement residenceCountry;
		@ButtonType()
		@FindBy(xpath = "(//button[normalize-space(.)='Save' and @title='Save' and @type='button'])[1]")
		public WebElement saveDA;
	}

	@FindByBlock(label = "Locations (Current, Place of Death, Usual Residence)", labelOperator = Operator.Equals, pageSectionStrategy = CaseViewSectionStrategy.class)
	public CaseViewSection caseViewSection;
			
}
