package pageobjects.CaseView;

import java.util.List;
import labellocators.*;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pagestrategies.*;
import com.provar.core.testapi.annotations.*;

@Page( title="Physician"                                
     , summary=""
     , relativeUrl=""
     , connection="Gaggan_QA"
     )             
public class Physician {

	@PageBlock()
	public static class CaseViewSection {

		@ButtonType()
		@FindBy(xpath = ".//button[normalize-space(.)='Physician' and @type='button']")
		public WebElement physician;
		@TextType()
		@FindByLabel(label = "Physician 1", controlLocator = "controlWithoutValue1", labelLocator = "labelLocator1", strategy = CaseViewLabelLocators.class)
		public WebElement physician1;
		@TextType()
		@FindByLabel(label = "Physician 1", controlLocator = "controlWithLookup", labelLocator = "labelLocator2", strategy = CaseViewLabelLocators.class)
		public WebElement Physician1Value;
		@TextType()
		@FindByLabel(label = "Physician 2", controlLocator = "controlWithLookup", labelLocator = "labelLocator2", strategy = CaseViewLabelLocators.class)
		public WebElement value;
		@TextType()
		@FindByLabel(label = "Physician Notes", controlLocator = "controlWithTextArea", labelLocator = "labelLocator2", strategy = CaseViewLabelLocators.class)
		public WebElement physicianNotes;
		@ButtonType()
		@FindBy(xpath = "(//button[normalize-space(.)='Save' and @title='Save' and @type='button'])[1]")
		public WebElement save;
		@PageWait.BackgroundActivity(timeoutSeconds = 60)
		@TextType()
		@FindByLabel(label = "Pronounced By", controlLocator = "controlWithoutValue1", labelLocator = "labelLocator1", strategy = CaseViewLabelLocators.class)
		public WebElement pronouncedBy;
		@PageWait.Field(timeoutSeconds = 10)
		@TextType()
		@FindByLabel(label = "Pronounced By", controlLocator = "controlWithoutValue2", labelLocator = "labelLocator2", strategy = CaseViewLabelLocators.class)
		public WebElement PronouncedBy1;
		@TextType()
		@FindByLabel(label = "Pronounced by Phone", controlLocator = "controlWithoutValue2", labelLocator = "labelLocator2", strategy = CaseViewLabelLocators.class)
		public WebElement PronouncedByPhone;
		@TextType()
		@FindByLabel(label = "Time of Death", controlLocator = "controlWithoutValue2", labelLocator = "labelLocator2", strategy = CaseViewLabelLocators.class)
		public WebElement timeOfDeath;
		@PageWait.BackgroundActivity(timeoutSeconds = 10)
		@TextType()
		@FindByLabel(label = "Date of Death", controlLocator = "controlWithValue1", labelLocator = "labelLocator2", strategy = CaseViewLabelLocators.class)
		public WebElement dateOfDeath;
		@TextType()
		@FindBy(xpath = "(//label[normalize-space(.)='Date of Death']/following-sibling::div//input[@type='text'])[2]")
		public WebElement dateOfDeathPro;
		@BooleanType()
		@FindByLabel(label = "Pronounced By Same as Reported By", controlLocator = "controlWithCheckbox", labelLocator = "labelLocator2", strategy = CaseViewLabelLocators.class)
		public WebElement pronouncedBySameAsReportedBy;
		@TextType()
		@FindByLabel(label = "Pronounced by Mobile", controlLocator = "controlWithoutValue2", labelLocator = "labelLocator2", strategy = CaseViewLabelLocators.class)
		public WebElement pronouncedByMobile;
		@ButtonType()
		@FindBy(xpath = "(//button[normalize-space(.)='Save' and @title='Save' and @type='button'])[2]")
		public WebElement savePB;
		
	}

	@FindByBlock(label = "Physician", labelOperator = Operator.Equals, pageSectionStrategy = CaseViewSectionStrategy.class)
	public CaseViewSection caseViewSection;
			
}
