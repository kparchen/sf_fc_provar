package pageobjects.CaseView;

import java.util.List;
import labellocators.*;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pagestrategies.*;
import com.provar.core.testapi.annotations.*;

@Page( title="ReportedBy"                                
     , summary=""
     , relativeUrl=""
     , connection="Gaggan_QA"
     )             
public class ReportedBy {

	@PageBlock()
	public static class CaseViewSection {

		@ButtonType()
		@FindBy(xpath = "//span[normalize-space(.)='Reported By' and @title='Reported By']")
		public WebElement ReportedBy;
		@TextType()
		@FindByLabel(label = "Reported By (Person Account)", controlLocator = "controlWithoutValue1", labelLocator = "labelLocator1", strategy = CaseViewLabelLocators.class)
		public WebElement reportedByPersonAccount;
		@TextType()
		@FindByLabel(label = "Reported By - Salutation", controlLocator = "controlWithPicklist", labelLocator = "labelLocator3", strategy = CaseViewLabelLocators.class)
		public WebElement ReportedBySalutation;
		@TextType()
		@FindByLabel(label = "Reported By - Middle Name", controlLocator = "controlWithoutValue2", labelLocator = "labelLocator2", strategy = CaseViewLabelLocators.class)
		public WebElement ReportedByMiddleName;
		@TextType()
		@FindByLabel(label = "Reported By - Home Phone", controlLocator = "controlWithoutValue2", labelLocator = "labelLocator2", strategy = CaseViewLabelLocators.class)
		public WebElement ReportedByHomePhone;
		@TextType()
		@FindByLabel(label = "Reported By - Business Phone", controlLocator = "controlWithoutValue2", labelLocator = "labelLocator2", strategy = CaseViewLabelLocators.class)
		public WebElement ReportedByBusinessPhone;
		@TextType()
		@FindByLabel(label = "Reported By - Mobile Phone", controlLocator = "controlWithoutValue2", labelLocator = "labelLocator2", strategy = CaseViewLabelLocators.class)
		public WebElement ReportedByMobilePhone;
		@LinkType()
		@FindByLabel(label = "Reported By - Language", controlLocator = "controlWithPicklist", labelLocator = "labelLocator3", strategy = CaseViewLabelLocators.class)
		public WebElement ReportedByLanguage;
		@LinkType()
		@FindByLabel(label = "Reported By - Relationship", controlLocator = "controlWithPicklist", labelLocator = "labelLocator3", strategy = CaseViewLabelLocators.class)
		public WebElement ReportedByRelationship;
		@TextType()
		@FindByLabel(label = "Reported By - First Name", controlLocator = "controlWithoutValue2", labelLocator = "labelLocator2", strategy = CaseViewLabelLocators.class)
		public WebElement ReportedByFirstName;
		@TextType()
		@FindByLabel(label = "Reported By - Last Name", controlLocator = "controlWithoutValue2", labelLocator = "labelLocator2", strategy = CaseViewLabelLocators.class)
		public WebElement ReportedByLastName;
		@TextType()
		@FindByLabel(label = "Reported By - Email", controlLocator = "controlWithoutValue2", labelLocator = "labelLocator2", strategy = CaseViewLabelLocators.class)
		public WebElement ReportedByEmail;
		@TextType()
		@FindByLabel(label = "Reported By - Other Language", controlLocator = "controlWithoutValue2", labelLocator = "labelLocator2", strategy = CaseViewLabelLocators.class)
		public WebElement reportedByOtherLanguage;
		@TextType()
		@FindByLabel(label = "Reported By - Home Phone Notes", controlLocator = "controlWithTextArea", labelLocator = "labelLocator2", strategy = CaseViewLabelLocators.class)
		public WebElement ReportedByHomePhoneNotes;
		@TextType()
		@FindByLabel(label = "Reported By - Mobile Phone Notes", controlLocator = "controlWithTextArea", labelLocator = "labelLocator2", strategy = CaseViewLabelLocators.class)
		public WebElement ReportedByMobilePhoneNotes;
		@TextType()
		@FindByLabel(label = "Reported By - Business Phone Notes", controlLocator = "controlWithTextArea", labelLocator = "labelLocator2", strategy = CaseViewLabelLocators.class)
		public WebElement ReportedByBusinessPhoneNotes;
		@BooleanType()
		@FindByLabel(label = "Reported By - Contact Home Phone", controlLocator = "controlWithCheckbox", labelLocator = "labelLocator2", strategy = CaseViewLabelLocators.class)
		public WebElement ReportedByContactHomePhone;
		@BooleanType()
		@FindByLabel(label = "Reported By - Contact Business Phone", controlLocator = "controlWithCheckbox", labelLocator = "labelLocator2", strategy = CaseViewLabelLocators.class)
		public WebElement ReportedByContactBusinessPhone;
		@BooleanType()
		@FindByLabel(label = "Reported By - Contact Mobile Phone", controlLocator = "controlWithCheckbox", labelLocator = "labelLocator2", strategy = CaseViewLabelLocators.class)
		public WebElement ReportedByContactMobilePhone;
		@TextType()
		@FindByLabel(label = "Reported By (Person Account)", controlLocator = "controlWithLookup", labelLocator = "labelLocator2", strategy = CaseViewLabelLocators.class)
		public WebElement ReportedByPA;
		@ButtonType()
		@FindBy(xpath = ".//button[normalize-space(.)='Save' and @title='Save' and @type='button']")
		public WebElement save;
		
	
	}

	@FindByBlock(label = "Reported By", labelOperator = Operator.Equals, pageSectionStrategy = CaseViewSectionStrategy.class)
	public CaseViewSection caseViewSection;
			
}
