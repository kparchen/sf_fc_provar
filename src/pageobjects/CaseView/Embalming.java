package pageobjects.CaseView;

import java.util.List;
import labellocators.*;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pagestrategies.*;
import com.provar.core.testapi.annotations.*;
import com.provar.plugins.forcedotcom.core.ui.pagecontrols.aura.ui.AuraUiInputDataTime;

@Page( title="Embalming"                                
     , summary=""
     , relativeUrl=""
     , connection="Gaggan_QA"
     )             
public class Embalming {

	@PageBlock()
	public static class CaseViewSection {

		@ButtonType()
		@FindBy(xpath = ".//button[normalize-space(.)='Embalming' and @type='button']")
		public WebElement embalming;
		@TextType()
		@FindByLabel(label = "Embalming Statement Language", controlLocator = "controlWithoutValue1", labelLocator = "labelLocator1", strategy = CaseViewLabelLocators.class)
		public WebElement embalmingStatementLanguage;
		@LinkType()
		@FindByLabel(label = "Embalming Statement Language", controlLocator = "controlWithPicklist", labelLocator = "labelLocator3", strategy = CaseViewLabelLocators.class)
		public WebElement embalmingLanguage;
		@PageWait.Field(timeoutSeconds = 10)
		@ButtonType()
		@FindBy(xpath = "(//button[normalize-space(.)='Save' and @title='Save' and @type='button'])[1]")
		public WebElement SaveEL;
		@PageWait.Field(timeoutSeconds = 10)
		@TextType()
		@FindByLabel(label = "Embalming Statement Read", controlLocator = "controlWithoutValue1", labelLocator = "labelLocator1", strategy = CaseViewLabelLocators.class)
		public WebElement embalmingRead;
		@LinkType()
		@FindByLabel(label = "Embalming Statement Read", controlLocator = "controlWithPicklist", labelLocator = "labelLocator3", strategy = CaseViewLabelLocators.class)
		public WebElement embalmingRead1;
		@LinkType()
		@FindByLabel(label = "Embalming Decision", controlLocator = "controlWithPicklist", labelLocator = "labelLocator3", strategy = CaseViewLabelLocators.class)
		public WebElement embalmingDecision;
		@PageWait.Field(timeoutSeconds = 10)
		@TextType()
		@FindByLabel(label = "Embalming Decision Given By", controlLocator = "controlWithLookup", labelLocator = "labelLocator2", strategy = CaseViewLabelLocators.class)
		public WebElement EmbalmingDecisionGivenBy;
		@PageWait.Field(timeoutSeconds = 10)
		@TextType()
		@FindByLabel(label = "Embalming Decision Given By: City", controlLocator = "controlWithoutValue2", labelLocator = "labelLocator2", strategy = CaseViewLabelLocators.class)
		public WebElement embalmingDecisionGivenByCity;
		@PageWait.Field(timeoutSeconds = 10)
		@TextType()
		@FindByLabel(label = "Embalming Decision Given By: State", controlLocator = "controlWithoutValue2", labelLocator = "labelLocator2", strategy = CaseViewLabelLocators.class)
		public WebElement embalmingDecisionGivenByState;
		@PageWait.Field(timeoutSeconds = 10)
		@TextType()
		@FindByLabel(label = "Embalming Decision Given By: Zip", controlLocator = "controlWithoutValue2", labelLocator = "labelLocator2", strategy = CaseViewLabelLocators.class)
		public WebElement embalmingDecisionGivenByZip;
		@TextType()
		@FindByLabel(label = "Embalming Decision Obtained By", controlLocator = "controlWithLookup", labelLocator = "labelLocator2", strategy = CaseViewLabelLocators.class)
		public WebElement EmbalmingDecisionObtainedBy;
		@TextType()
		@FindByLabel(label = "Date", controlLocator = "controlWithoutValue2", labelLocator = "labelLocator5", strategy = CaseViewLabelLocators.class)
		public WebElement EmbalmingDate;
		@TextType()
		@FindByLabel(label = "Time", controlLocator = "controlWithoutValue2", labelLocator = "labelLocator5", strategy = CaseViewLabelLocators.class)
		public WebElement EmbalmingTime;
	}

	@FindByBlock(label = "Embalming", labelOperator = Operator.Equals, pageSectionStrategy = CaseViewSectionStrategy.class)
	public CaseViewSection caseViewSection;
			
}
