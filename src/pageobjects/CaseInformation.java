package pageobjects;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.AuraBy;
import com.provar.core.testapi.annotations.ButtonType;
import com.provar.core.testapi.annotations.FindByLabel;
import com.provar.core.testapi.annotations.LabelType;
import com.provar.core.testapi.annotations.LinkType;
import com.provar.core.testapi.annotations.Page;
import com.provar.core.testapi.annotations.TextType;

@Page( title="CaseInformation"                                
     , summary=""
     , relativeUrl=""
     , connection="kparchen_QA"
     )             
public class CaseInformation {

	public CaseInformation(WebDriver driver) {
		drivers = driver;
	}

	public WebDriver drivers;
	@TextType()
	@FindByLabel(label = "Date of Death", labelType = LabelType.SalesforceLightningDesignSystem)
	public WebElement dateOfDeath;
	@TextType()
	@FindByLabel(label = "Account Name", labelType = LabelType.SalesforceLightningDesignSystem)
	public WebElement accountName;
	@TextType()
	@FindBy(xpath = "//span[text()=\"Account Name\"]/following::div[position()=1]/span/span")
	public WebElement accountName1;
	@TextType()
	@FindByLabel(label = "First Name")
	public WebElement firstName;
	@TextType()
	@FindByLabel(label = "Last Name")
	public WebElement lastName;
	@TextType()
	@FindByLabel(label = "Phone Number")
	public WebElement phoneNumber;
	@TextType()
	@FindByLabel(label = "MobilePhone")
	public WebElement mobilePhone;
	@TextType()
	@FindByLabel(label = "Email")
	public WebElement email;
	@TextType()
	@FindBy(xpath = "//div[text()=\"Description\"]/following::textarea[1]")
	public WebElement description;
	@ButtonType()
	@FindByLabel(label = "Next")
	public WebElement next;
	@TextType()
	@FindBy(xpath = "//*[text()='YOUR FLOW FINISHED']")
	public WebElement acceptOwnershipResultMsg;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Queues']")
	public WebElement queues;
	@TextType()
	@FindByLabel(label = "Owner")
	public WebElement newOwn;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='Pick an object']")
	public WebElement ownerTrigger;
	@LinkType()
	@FindBy(linkText = "People")
	public WebElement people;
	@TextType()
	@FindBy(xpath = "//*[@class='tabBar slds-grid']")
	public WebElement tabBar;
	public void closeTabs() {
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		Actions action = new Actions(drivers);
		action.sendKeys(tabBar, Keys.chord(Keys.SHIFT, "w")).perform();
	}

	@ButtonType()
	@AuraBy(componentXPath = "//lightning:button[@label= 'Close All']")
	public WebElement closeAll;
	@ButtonType()
	@FindByLabel(label = "Finish")
	public WebElement finish;
			
}
