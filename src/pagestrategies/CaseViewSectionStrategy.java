package pagestrategies;

import org.openqa.selenium.support.FindBy;
import com.provar.core.testapi.annotations.PageSectionLocatorStrategy;
import com.provar.core.testapi.annotations.StrategyLocator;



@PageSectionLocatorStrategy(title="SF Lightning Page Case View Section", sectionName ="CaseViewSection",
sectionLocators = {
		@StrategyLocator(findBy = @FindBy(xpath="//section[contains(@class,'section')]"),name="Section"),
},
labelLocators = {
		@StrategyLocator(findBy = @FindBy(xpath="div/h2/button/span/@title"),name="Label"),
},
expanders = {
		@StrategyLocator(findBy = @FindBy(xpath=".//h2//span[not(contains(@class,'header'))]"), appliesIf = @FindBy(xpath="self::section[not(contains(@class,'open'))]"),name="Expander"),
		
}
)

public class CaseViewSectionStrategy {}




